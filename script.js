let student = [];

function addStudent(name) {
	student.push(name);
}

function printStudent() {
	student.forEach(
		function(name) {
			console.log(name);
		}
	)
}

function countStudents() {
	console.log(student.length);
}

function findStudent(name) {
	const isPresent = student.includes(name);

	if (isPresent) {
		console.log(name);
	} else {
		console.log(`${name} is not on the list.`);
	}
}


// Invocation
addStudent("Christine");
addStudent("Jeremiah");
addStudent("Dave");

printStudent();

countStudents();

findStudent("Jeremiah");
findStudent("Emil");
